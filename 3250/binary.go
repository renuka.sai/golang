package main

import "fmt"

func binarySearch(num int, array []int) bool {

	low := 0
	high := len(array) - 1

	for low <= high {
		mid := (low + high) / 2

		if array[mid] < num {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}

	if low == len(array) || array[low] != num {
		return false
	}

	return true
}

func main() {
	arr := []int{1, 4, 9, 19, 26, 33, 44, 56, 67, 76, 86, 92}
	fmt.Println(binarySearch(25, arr))
	fmt.Println(binarySearch(19, arr))
}t 
