/*

Pointers in Golang and 
calling function (foo) from another file (foo.go)
in the same package

*/

package main

import (
	"fmt"
)

func func1() *int{
	var a int=5
	return &a
}

func main(){
	var p *int=func1()
	(*p)++
	fmt.Print(*p)
	foo()
}