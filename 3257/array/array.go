package main

import "fmt"

func array() {

	array1 := [5]int{4, 5, 7}
	var array2 = [5]int{4, 5, 7}
	if array1 == array2 {
		fmt.Println("Array1 and Array2 are equal")
	} else {
		fmt.Println("Array1 and Array2 are not equal")
	}
	for i := 0; i < len(array1); i++ {
		fmt.Printf("%d ", array1[i])
	}
	fmt.Println()
	fmt.Println(array1)
}
func main() {
	array()
}
