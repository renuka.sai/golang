//It takes input from user and prints its factorial.

package main

import "fmt"

var factorial int


func Fact(n int) int {
    factorial = 1
    for i := 1; i <= n; i++ {
        factorial = factorial * i
    }
    return factorial
}


func main(){   
    var n int
    fmt.Print("Enter number btw 0-50")
    fmt.Scan(&n)   
    fmt.Print("Factorial= ",Fact(n))
}
