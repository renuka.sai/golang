package main

import "fmt"

func main() {
	arr := []int{5, 10, 15, 20, 25, 30}
	var target int = 30
	var begin int = 0
	var mid int = 0
	var end int = len(arr) - 1
	var ind int = -1
	for begin <= end {
		mid = begin + (end-begin)/2
		if arr[mid] == target {
			ind = mid
			break
		} else if arr[mid] < target {
			begin = mid + 1
		} else if arr[mid] > target {
			end = mid - 1
		}
	}
	if ind == -1 {
		fmt.Println("element not found")
	} else {
		fmt.Println("element found at", ind)
	}
}
