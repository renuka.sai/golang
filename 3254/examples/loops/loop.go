// playing with loops

package main

import "fmt"

func main() {
	array1 := [5]int{4, 5, 7}
	array2 := [5]int{4, 5, 7}
	fmt.Println(array1)
	fmt.Println(array2)

	for i := 0; i < len(array1); i++ {
		fmt.Printf("%d ", array1[i])
	}
	fmt.Println()
	fmt.Println(array1)
}
