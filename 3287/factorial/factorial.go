//program to find factorial of a number

package main

import "fmt"

func factorial(num int) int {
	if num == 1 || num == 0 {
		return num
	}
	return num * factorial(num-1)
}

func main1() {

	fmt.Println("Factorial of 5 : ", factorial(5))
	fmt.Println("Factorial of 9 : ", factorial(9))
	fmt.Println("Factorial of 3 : ", factorial(3))
}
