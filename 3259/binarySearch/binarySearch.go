package main

import "fmt"

func binarySearch(key int, arr []int) int {

	low := 0
	high := len(arr) - 1

	for low <= high {
		median := (low + high) / 2

		if arr[median] < key {
			low = median + 1
		} else {
			high = median - 1
		}
	}

	if low == len(arr) || arr[low] != key {
		return 0
	}

	return 1
}

func main() {
	arr := []int{10, 15, 22, 34, 45, 56, 67, 78, 89, 90, 100}

	fmt.Println("Enter the number you want to search : ")
	var num int
	var ans int
	// Taking input from user
	fmt.Scanln(&num)

	ans = binarySearch(num, arr)

	if ans == 1 {
		fmt.Println("Yes. The number is present in the array")
	} else {
		fmt.Println("No. The number is not present in the array")
	}

}
